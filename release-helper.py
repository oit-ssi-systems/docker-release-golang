#!/usr/bin/env python3
import sys
import argparse
import logging
import json
import subprocess
import os


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(
        description="Do a release using a directory of assets",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )
    parser.add_argument("-n", "--name", type=str, help="Name of release", required=True)
    parser.add_argument("dir", help="Directory containing releases")
    return parser.parse_args()


def main():
    args = parse_args()
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

    ci_commit_tag = os.environ["CI_COMMIT_TAG"]
    registry_url = os.environ["PACKAGE_REGISTRY_URL"]
    cmd = [
        "/usr/local/bin/release-cli",
        "create",
        "--name",
        args.name,
        "--tag-name",
        ci_commit_tag,
    ]
    for item in os.listdir(args.dir):
        asset_item = {"name": item, "url": f"{registry_url}/{item}"}
        cmd.extend(["--assets-link", json.dumps(asset_item)])
    subprocess.call(cmd, env=os.environ)

    return 0


if __name__ == "__main__":
    sys.exit(main())

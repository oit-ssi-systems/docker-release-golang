FROM ubuntu:20.04
ENV DEBIAN_FRONTEND 'noninteractive'
ENV GO111MODULES 'on'

  # hadolint ignore=DL3008,DL3005
RUN apt-get update && \
      apt-get -y install --no-install-recommends \
        software-properties-common  \
        bzip2 \
        make \
        curl \
        ca-certificates \
        git \
        golang \
        python3-pip \
        python3-requests &&\
      rm -rf /var/lib/apt/lists/* && \
      go get gitlab.com/gitlab-org/release-cli/cmd/release-cli && \
      mv /root/go/bin/release-cli /usr/local/bin/

COPY release-helper.py /usr/local/bin/release-helper.py
